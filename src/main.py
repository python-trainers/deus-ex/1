from logging import DEBUG, ERROR, basicConfig

from trainerbase.config import trainerbase_config
from trainerbase.main import run

from gui import run_menu
from injections import ammo_hook, health_hook
from scripts import script_engine


def on_startup():
    logging_level = DEBUG if trainerbase_config["logging"]["level"] == "DEBUG" else ERROR
    basicConfig(level=logging_level)

    ammo_hook.inject()
    health_hook.inject()
    script_engine.start()


def on_shutdown():
    script_engine.stop()


if __name__ == "__main__":
    run(run_menu, on_startup, on_shutdown)
