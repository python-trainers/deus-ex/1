from trainerbase.gameobject import GameInt
from trainerbase.memory import Address

from memory import ammo_array_manager


class Health:
    def __init__(self) -> None:
        self.head = GameInt(0, value_range=(2, 9_999))
        self.torso = GameInt(0, value_range=(2, 9_999))
        self.left_arm = GameInt(0, value_range=(2, 9_999))
        self.right_arm = GameInt(0, value_range=(2, 9_999))
        self.left_leg = GameInt(0, value_range=(2, 9_999))
        self.right_leg = GameInt(0, value_range=(2, 9_999))

    def update_from_base_address(self, new_base_address: Address):
        self.head.address = new_base_address + 0 * GameInt.ctype_size
        self.torso.address = new_base_address + 1 * GameInt.ctype_size
        self.left_leg.address = new_base_address + 2 * GameInt.ctype_size
        self.right_leg.address = new_base_address + 3 * GameInt.ctype_size
        self.left_arm.address = new_base_address + 4 * GameInt.ctype_size
        self.right_arm.address = new_base_address + 5 * GameInt.ctype_size


ammo_array = [GameInt(0) for _ in range(len(ammo_array_manager))]
health = Health()
