from trainerbase.common.helpers import ASMArrayManager
from trainerbase.memory import POINTER_SIZE, get_module_address


core_dll = get_module_address("Core.dll")

ammo_array_manager = ASMArrayManager(15, POINTER_SIZE, [0x0])
health_array_manager = ASMArrayManager(500, POINTER_SIZE, [0x0])
