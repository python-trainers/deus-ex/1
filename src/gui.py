from typing import override

from dearpygui import dearpygui as dpg
from trainerbase.gameobject import GameObject
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.types import AbstractUIComponent

from objects import Health, ammo_array, health


class HealthUI(AbstractUIComponent):
    def __init__(self, health: Health) -> None:
        self.health = health

    @override
    def add_to_ui(self) -> None:
        add_components(
            self.create_limb_health(self.health.head, "Head"),
            self.create_limb_health(self.health.torso, "Torso"),
            self.create_limb_health(self.health.left_arm, "Left Arm"),
            self.create_limb_health(self.health.right_arm, "Right Arm"),
            self.create_limb_health(self.health.left_leg, "Left Leg"),
            self.create_limb_health(self.health.right_leg, "Right Leg"),
        )

        return super().add_to_ui()

    def create_limb_health(self, game_object: GameObject, label: str) -> GameObjectUI:
        return GameObjectUI(game_object, label, set_hotkey="PageUp", default_setter_input_value=1000)


@simple_trainerbase_menu("Deus Ex", 700, 450)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Health"):
            add_components(HealthUI(health))

        with dpg.tab(label="Last Updated Game Values"):
            for i, ammo_object in enumerate(ammo_array, start=1):
                add_components(GameObjectUI(ammo_object, f"Value #{i}", default_setter_input_value=999))
