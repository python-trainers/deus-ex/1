from collections.abc import Sequence
from logging import getLogger
from typing import Callable

from trainerbase.common.helpers import ASMArrayManager
from trainerbase.gameobject import GameInt
from trainerbase.memory import Address
from trainerbase.scriptengine import Script, ScriptEngine

from memory import ammo_array_manager, health_array_manager
from objects import Health, ammo_array, health


logger = getLogger(__name__)

script_engine = ScriptEngine()


class AmmoAddressesUpdater:
    def __init__(self, ammo_array: Sequence, ammo_array_manager: ASMArrayManager) -> None:
        self.ammo_array = ammo_array
        self.ammo_array_manager = ammo_array_manager
        self.unique_ammo_addresses: set[int] = set()

    def __call__(self):
        new_addresses = {
            address.resolve()
            for address in self.ammo_array_manager
            if 0 < GameInt(address, is_tracked=False).value < 1_000_000
        }

        if len(self.unique_ammo_addresses) + len(new_addresses) > len(self.ammo_array):
            self.unique_ammo_addresses.clear()

        self.unique_ammo_addresses.update(new_addresses)

        for address, ammo in zip(sorted(self.unique_ammo_addresses), self.ammo_array):
            ammo.address = Address(address)

        for ammo in self.ammo_array:
            if not (0 < ammo.value < 1_000_000):
                ammo.address = Address(0)


class HealthAddressUpdater:
    """
    Updates the address of the player's health structure.

    CodeInjection health_hook updates all HP values on the map.
    This script tries to find the player's HP in the HP array.

    The idea is that for one entity (player or NPC) there are 6 integer values, but up to the HP of the player's head.
    we always see 3 pointers (the value should be > 1_000_000). Before these pointers should be
    at least 3 zeros.

    Struct format [
        ...,
        0, 0, 0,
        pointer > 1_000_000, pointer > 1_000_000, pointer > 1_000_000,
        head, torso, left_leg, right_leg, left_arm, right_arm,
        ...,
    ]
    """

    def __init__(self, health: Health, health_array_manager: ASMArrayManager) -> None:
        self.health = health
        self.health_array_manager = health_array_manager
        self.bad_addresses = set()

    def __call__(self):
        for address in self.health_array_manager:
            self.try_to_update_health_address(address)

    def try_to_update_health_address(self, address: Address) -> None:
        last_updated_health_address = address.resolve()

        if last_updated_health_address in self.bad_addresses:
            return

        if not self.check_if_value_in_valid_hp_range(last_updated_health_address):
            self.bad_addresses.add(last_updated_health_address)
            logger.debug(f"{hex(last_updated_health_address)} is a bad address (1)")
            return

        last_anchor_pointer_address = self.find_last_pointer_anchor_address(last_updated_health_address)

        if last_anchor_pointer_address is None:
            self.bad_addresses.add(last_updated_health_address)
            logger.debug(f"{hex(last_updated_health_address)} is a bad address (2)")
            return

        if not self.validate_ints_in_range(
            last_anchor_pointer_address - GameInt.ctype_size * 5,
            3,
            lambda x: x == 0,
        ) or not self.validate_ints_in_range(
            last_anchor_pointer_address - GameInt.ctype_size * 3,
            3,
            lambda x: x > 1_000_000,
        ):
            self.bad_addresses.add(last_updated_health_address)
            logger.debug(f"{hex(last_updated_health_address)} is a bad address (3)")

        logger.debug(f"{hex(last_updated_health_address)} is HP address")
        self.health.update_from_base_address(Address(last_anchor_pointer_address + GameInt.ctype_size))

    def check_if_value_in_valid_hp_range(self, last_updated_health_address: int) -> bool:
        return 1 <= GameInt(last_updated_health_address, is_tracked=False).value <= 10_000

    def find_last_pointer_anchor_address(self, last_updated_health_address: int) -> int | None:
        for offset in range(0, -GameInt.ctype_size * 7, -GameInt.ctype_size):
            health_or_anchor_pointer = GameInt(
                Address(last_updated_health_address, add=offset),
                is_tracked=False,
            ).value

            if health_or_anchor_pointer > 1_000_000:  # anchor pointer
                return Address(last_updated_health_address, add=offset).resolve()

        return None

    def validate_ints_in_range(self, start: int, count: int, callback: Callable[[int], bool]) -> bool:
        anchor_value_address = start

        for offset in range(0, count * GameInt.ctype_size, GameInt.ctype_size):
            if not callback(GameInt(anchor_value_address + offset, is_tracked=False).value):
                return False

        return True


update_ammo_addresses = script_engine.register_script(
    Script(
        AmmoAddressesUpdater(ammo_array, ammo_array_manager),
        enabled=True,
    ),
)

update_health_addresses = script_engine.register_script(
    Script(
        HealthAddressUpdater(health, health_array_manager),
        enabled=True,
    ),
)
