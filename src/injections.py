from trainerbase.codeinjection import AllocatingCodeInjection

from memory import ammo_array_manager, core_dll, health_array_manager


ammo_hook = AllocatingCodeInjection(
    core_dll + 0x345DC,
    f"""
        cmp dword [edi], 1000000
        jge original_code

        cmp dword [edi], 0
        jle original_code

        {ammo_array_manager.generate_asm_append_code("edi")}

        original_code:

        mov eax, [edi]
        inc ecx
        sub eax, edx
    """,
    original_code_length=5,
)

health_hook = AllocatingCodeInjection(
    core_dll + 0x345E4,
    f"""
        cmp dword [edi], 0
        je original_code

        {health_array_manager.generate_asm_append_code("edi")}

        original_code:

        mov [edi], eax
        mov edi, eax
        mov eax, [esp + 0x14]
    """,
    original_code_length=8,
)
